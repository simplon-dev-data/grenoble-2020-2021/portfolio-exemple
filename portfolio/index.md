---
hide:
  - navigation
---

![Richard Hendricks](./static/face.jpg){: style="width: 20%" }

# Richard Hendricks

> Développeur Data

Mes centres d'intérêts professionnels actuels inclus l'ingénierie data,
l'intégration continue, le déploiement continu, la cryptographie et la pédagogie active.

---

[:fontawesome-brands-linkedin:{: style="transform: scale(1.5); margin: 0 0.5rem;" }](https://www.linkedin.com/in/richard-hendricks-23524799/ "LinkedIn")
[:fontawesome-brands-gitlab:{: style="transform: scale(1.5); margin: 0 0.5rem;" }](https://gitlab.com "GitLab")
[:fontawesome-brands-github:{: style="transform: scale(1.5); margin: 0 0.5rem;" }](https://github.com/piedpiperRichard "GitHub")

---

## Projets

Vous pouvez retrouver mes projets sur [:fontawesome-brands-github: GitHub](https://github.com/piedpiperRichard)
et [:fontawesome-brands-gitlab: GitLab](https://gitlab.com).

### [Data Fools](https://simplon-dev-data.gitlab.io/data-fools/)

> Avril 2020

Un poisson d'avril sous forme de faux formulaire web recueillant beaucoup trop d'informations personnelles par rapport à l'objectif énoncé.
Les objectifs étaient de :

- Sensibiliser les apprenants vis à vis des données personnelles sensibles
- Observer les potentielles requêtes réseau contenant de l'information sensible

_Compétences_ : développement web, déploiement continu

![Data Fools](./static/projet-data-fools.png){: style="width: 20%" }

### [Data Trivia](https://simplon-dev-data.gitlab.io/data-trivia/)

> 2019 - présent

Un jeu de trivia en ligne permettant de faciliter l'apprentissage des
apprenants en formation Simplon Dev Data. L'application est entièrement
côté client, générée statiquement et déployée automatiquement à chaque
ajout de questions.

_Compétences_ : développement web, intégration continue, déploiement continu

_Technologies_ : Nuxt.js, JAMStack, YAML

![Data Trivia](./static/projet-data-trivia.png){: style="width: 20%" }

### [Pied-piper Coin](https://github.com/portfolio-api-demo/pied-piper-coin)

> 2017 - 2019

La monnaie du web ouvert.

_Technologies_ : TypeScript, Go, Node.js, React


### [Middle-Out Compression](https://github.com/portfolio-api-demo/middle-out-compression)

> 2014 - 2019

Un algorithme pour rendre les fichiers plus petits.
Vendu à Hooli pour 1 milliard de dollars.

_Technologies_ : Go, C++

---

## Formations

### [Simplon Développeur.se Data](https://simplon.co/formation/developpeur-data/9)

> Novembre 2020 - Septembre 2021, Grenoble

De l’analyse du besoin à la data visualisation, en passant par la récolte et le traitement des données, le·la développeur·se data conçoit et exploite les bases de données.

Le·la développeur·se data gère l'ensemble du cycle de vie de la donnée, de la donnée brute jusqu'à la livraison de données utilisables. Il s’agit d'un·e technicien·ne capable d'appréhender n'importe quel type de format de données, de les stocker en base de données, les interroger et de les servir, avec un rendu visuel ou un support adapté pour un usage tiers. Il·elle peut être amené·e à automatiser des processus d'acquisition, d'import, d'extraction et de visualisation de données. Il·elle est le garant de la qualité, de l'intégrité et de la cohérence des données avant et après traitement.

Le métier de développeur·se data s’articule alors autour de 2 activités principales :

- Développer une base de données
- Exploiter une base de données
